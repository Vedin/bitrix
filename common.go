package bitrix

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	u "net/url"
	"time"
)

type Client struct {
	Client_id     string
	Client_secret string

	Delay      time.Duration
	HttpClient *http.Client

	HttpDebug  bool
	HttpWriter io.Writer

	autoRefresh     bool
	autoRefreshFunc func(string) error

	reqChan  chan *http.Request
	respChan chan *http.Response
	errChan  chan error
}

func (c *Client) Start() {
	c.reqChan = make(chan *http.Request)
	c.respChan = make(chan *http.Response)
	c.errChan = make(chan error)
	go c.query()
}

func (c *Client) CheckAccess(domain, access_token string) bool {
	ok, _, _ := c.UrlMethod(domain, access_token, "server.time", nil)
	return ok
}

func (c *Client) UrlMethod(domain, access_token, method string, params map[string]string) (bool, []byte, error) {
	if c.autoRefresh {
		err := c.autoRefreshFunc(domain)
		if err != nil {
			return false, nil, errors.New(fmt.Sprintf("query error: %s", err))
		}
	}

	url := "https://" + domain + "/rest/" + method
	v := u.Values{}
	v.Add("auth", access_token)
	if params != nil {
		for key, val := range params {
			v.Add(key, val)
		}
	}
	var err error
	count := 0
	bts := []byte{}
	for {
		count++
		if count > 5 {
			return false, bts, fmt.Errorf("query error: %s", err)
		}
		req, err := http.NewRequest("GET", url+"?"+v.Encode(), nil)
		if err != nil {
			return false, nil, errors.New(fmt.Sprintf("query error: %s", err))
		}

		if c.HttpDebug {
			dump, err := httputil.DumpRequestOut(req, true)
			if err != nil {
				c.HttpWriter.Write([]byte("dump request error: " + err.Error()))
			}
			c.HttpWriter.Write(dump)
			c.HttpWriter.Write([]byte("\r\n\r\n"))
		}
		c.reqChan <- req
		var resp *http.Response
		select {
		case resp = <-c.respChan:
		case err = <-c.errChan:
			continue
		}
		defer resp.Body.Close()

		if c.HttpDebug {
			dump, err := httputil.DumpResponse(resp, true)
			if err != nil {
				c.HttpWriter.Write([]byte("dump response error: " + err.Error()))
			}
			c.HttpWriter.Write(dump)
			c.HttpWriter.Write([]byte("\r\n\r\n"))
		}
		bts, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			continue
		}
		if resp.StatusCode != 200 {
			return false, bts, nil
		}
		break
	}

	return true, bts, nil
}

func (c *Client) Method(domain, access_token, method string, params interface{}) (bool, []byte, error) {
	if c.autoRefresh {
		err := c.autoRefreshFunc(domain)
		if err != nil {
			return false, nil, errors.New(fmt.Sprintf("query error: %s", err))
		}
	}

	url := "https://" + domain + "/rest/" + method + "?auth=" + access_token
	// log.Println(url)
	var bts []byte
	var err error
	if params != nil {
		bts, err = json.Marshal(params)
		if err != nil {
			return false, nil, errors.New(fmt.Sprintf("query error: %s", err))
		}
	}

	count := 0
	rbts := []byte{}
	for {
		count++
		if count > 5 {
			return false, rbts, fmt.Errorf("query error: %s", err)
		}
		body := bytes.NewReader(bts)
		req, err := http.NewRequest("POST", url, body)
		if err != nil {
			return false, nil, errors.New(fmt.Sprintf("query error: %s", err))
		}
		req.Header.Set("Content-Type", "application/json")

		if c.HttpDebug {
			dump, err := httputil.DumpRequestOut(req, true)
			if err != nil {
				c.HttpWriter.Write([]byte("dump request error: " + err.Error()))
			}
			c.HttpWriter.Write(dump)
			c.HttpWriter.Write([]byte("\r\n\r\n"))
		}
		c.reqChan <- req
		var resp *http.Response
		select {
		case resp = <-c.respChan:
		case err = <-c.errChan:
			continue
		}
		defer resp.Body.Close()

		if c.HttpDebug {
			dump, err := httputil.DumpResponse(resp, true)
			if err != nil {
				c.HttpWriter.Write([]byte("dump response error: " + err.Error()))
			}
			c.HttpWriter.Write(dump)
			c.HttpWriter.Write([]byte("\r\n\r\n"))
		}
		rbts, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			continue
		}
		if resp.StatusCode != 200 {
			return false, rbts, nil
		}
		break
	}

	return true, rbts, nil
}

func (c *Client) query() {
	ch := make(chan struct{})
	go func(ch chan struct{}) {
		for {
			time.Sleep(c.Delay)
			<-ch
		}
	}(ch)

	for req := range c.reqChan {
		client := http.Client{
			Timeout: time.Second * 30,
		}
		ch <- struct{}{}
		resp, err := client.Do(req)
		if err != nil {
			c.errChan <- err
		} else {
			c.respChan <- resp
		}
	}

}

func (c *Client) SetAutoRefresh(check func(domain string) (bool, string), update func(string, *OAuthResp)) {
	c.autoRefresh = true
	c.autoRefreshFunc = func(domain string) error {
		if ok, refresh_token := check(domain); ok {
			resp, err := c.Refresh(refresh_token)
			if err != nil {
				return errors.New(fmt.Sprintf("auto refresh error: %s", err))
			}
			update(domain, resp)
		}
		return nil
	}
}
